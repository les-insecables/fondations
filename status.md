# Statuts

## Création et objet

### <a name="article_1">Article 1</a>
Le club d’investissement **Les insécables** est constitué à compter du **01/01/2017/** entre les soussignés, membres fondateurs, sous la forme d’une indivision, conformément aux articles 815 et suivants, 1873-1 et suivants du Code Civil. Une dissolution anticipée peut intervenir suivant les modalités prévues à [l’article 19](#article_19) ci-après.

### <a name="article_2">Article 2</a>
Le club a pour objet l’éducation et l’information de chacun de ses membres par la constitution et la gestion en commun d’un portefeuille collectif de valeurs mobilières déposé auprès de `____________________` (intermédiaire financier).

### <a name="article_3">Article 3</a>
**Le club doit réunir cinq membres au moins et vingt membres au plus**. Ces membres sont obligatoirement des personnes physiques. Les membres mineurs devront remettre au Président du club (ce dernier étant obligatoirement majeur), une autorisation parentale datée et signée, qui sera transmise à l’intermédiaire financier auprès duquel est ouvert le compte du club.

### Article 4
**Chaque membre ne peut faire partie que d’un seul club d’investissement**.

### Article 5
**Les membres s’obligent à respecter les dispositions fiscales en vigueur.**
Il est rappelé qu’en vertu de la
[loi n°78-688 du 5 juillet 1978](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=LEGIARTI000006317104&cidTexte=LEGITEXT000006068634&dateTexte=19780706) et le rappel dans la documentation de base [DB 5I422](http://archives-bofip.impots.gouv.fr/bofip-A/g1/27760-AIDA/28924-AIDA/29029-AIDA/29047-AIDA/29049-AIDA.html),
seuls les gains nets retirés par les membres, à l’occasion de leur retrait ou de la dissolution du club d’investissement, sont soumis à taxation dans la mesure où le total des cessions effectuées au niveau du foyer fiscal, au cours de l’année d’imposition, y compris les sommes se rapportant aux retraits ou à la dissolution, excède le montant fixé par la loi. Chaque membre est imposable sur sa quote-part de dividendes et autres produits perçus au sein du club. Il bénéficie en contrepartie des abattements, crédits d’impôts et avoirs fiscaux attachés à sa quote-part des revenus.

## Ressources du Club et emplois des fonds versés
### Article 6
Chaque membre s’engage à verser régulièrement une somme permettant d’alimenter le portefeuille du club. **Le total des versements annuel par membre ne devra pas excéder : 5500 €**. (Instruction du 27 décembre 2000 - B.O.D.G.I. 5 1-01 n°7 du 10 janvier 2001).
Si plusieurs membres d’une même famille font partie du même club d’investissement, les maxima de versements autorisés s’apprécient au niveau du foyer fiscal.

### Article 7
La contribution de chaque membre est exprimée sous forme de quotes-parts de droits indivis (prorata de droits aux actifs du club).

### Article 8
Les sommes recueillies par le club sont destinées exclusivement à l’acquisition et à la gestion d’un portefeuille de valeurs mobilières.

Seuls les investissements directs sont éligibles (pas de produit côté en bourse, la somme investie devra être disponible pour l’entreprise).

La nature de l’investissement (capital, prêt, compte courant d’associé…) sera évaluée au cas-par-cas.

## Fonctionnement et administration du Club
### Article 9
**Les membres du club se réunissent en principe quatre fois par an**. La date de chaque réunion est précisée et consignée dans le procès-verbal lors de la réunion qui précède.

*Des réunions exceptionnelles peuvent être tenues à la demande de la moitié des membres.*

La réunion trimestrielle permet de :

* Acter des décisions prises selon les modalités de [l’article_10](#article_10) ;
* Évaluer les fonds disponibles à investir lors du trimestre suivant ;
* Évaluer sur proposition du trésorier de la valeur de chaque investissement en cours ;
* Nommer pour chaque investissement un référent qui servira de relai entre l’entreprise et le club.

### <a name="article_10">Article 10</a>
La répartition des voix est **une voix par membre**. Les décisions sont prises à la **majorité absole des voix exprimées**.

Les votes sont publics au sein des membres du club et en ligne.

Un vote doit être ouvert au moins deux semaines avant la réunion trimestrielle qui permettra d’acter la prise de décision.

Tout vote portant sur un investissement doit être accompagné d’un
montant d’investissement.

### Article 11
Les membres du club procèdent, au cours de la réunion constitutive, à l’élection d’un Bureau choisi parmi les membres. Ce Bureau comprend un Président, un Secrétaire et un Trésorier, élus pour un an, et dont les mandats sont renouvelables par décision des membres du club statuant dans les conditions de [l’article 10](#article_10).

**Le Président** dirige les débats et transmet, sous sa responsabilité, les décisions d’investissement et de désinvestissement. Il représente le club auprès des tiers en toutes circonstances.

Il peut, en cas d’urgence grave et justifiable, sous réserve d’en rendre compte lors de la réunion suivante, prendre toutes mesures à l’effet de conserver et de sauvegarder les avoirs du club. En cas d’empêchement, le Président peut déléguer ses pouvoirs à l’un des membres du Bureau.

**Le Trésorier** s’assure du versement des contributions de chaque membre et tient la comptabilité des avoirs en espèces et en titres. Il peut, par délégation du Président, transmettre à l’intermédiaire financier les décisions d’investissement et de désinvestissement du club.

**Le Secrétaire** rédige les procès-verbaux des séances et en remet copie à chacun des membres.

### Article 12
Il est ouvert, auprès de l’intermédiaire financier visé à [l’article 2](#article_2), un compte au nom du club sur lequel sont virés les versements des membres. Les fonds en attente de placement et les valeurs mobilières acquises doivent être déposés auprès de l’intermédiaire financier. Le Président portera à la connaissance des membres du club les modalités de fonctionnement du compte du club et les conditions tarifaires appliquées par l’intermédiaire financier. Pour l’ouverture du compte, seront déposés auprès de l’intermédiaire financier :

* une copie du présent document revêtu de la signature de tous les membres du club.
* un exemplaire du procès-verbal de la réunion ayant nommé le Président, certifié conforme par le Secrétaire et le Trésorier.

**L’intermédiaire financier** ne peut être tenu pour responsable des versements dûs par chacun des membres du club. Il ne lui appartient pas non plus de s’assurer que ces versements sont effectués à bonne date. Il reçoit les instructions d’investissement et de désinvestissement du club et les exécute. Il adresse les avis d’exécution ou d’opéré au Président ou au Trésorier. Conformément à l’état de répartition que lui communique le Président, il établit, pour chaque membre du club, l’imprimé fiscal unique de fin d’année. Il est de la responsabilité du Président de communiquer à l’intermédiaire financier, par remise d’une copie de l’avenant aux statuts numéroté, tout changement intervenant dans les qualités des membres du club (changement d’adresse, nom marital...) ou dans la composition du club (départ ou admission) ou plus généralement toute information affectant l’établissement de l’imprimé fiscal unique.

### Article 13
Lors de ses réunions, le club peut demander la présence d’un représentant de son intermédiaire financier dont le rôle ne sera que consultatif. Ce dernier ne pourra prendre part aux votes.

## Admission — Démission — Décès — Exclusion
### Article 14
**L’admission de nouveaux membres**, dans les limites mentionnées à [l’article 3](#article_3), est soumise à l’approbation de l’unanimité des membres présents ou représentés.

### <a name="article_15">Article 15</a>
*Tout membre peut se retirer en respectant un préavis de un mois*. Sur accord de l’ensemble des membres, ce préavis peut être abrégé. Sa quote-part de droits, dans le portefeuille commun, lui sera alors remboursée sur la base de la valeur du portefeuille.

Le membre sortant fait son affaire, sans recours contre le club, de la déclaration éventuelle de ses gains en capital taxables au régime des plus-values mobilières.

### Article 16
*En cas de décès de l’un des membres*, l’indivision ne se poursuit pas avec les ayants droit du défunt. Dès que le décès est porté à la connaissance du club, la valeur de la quote-part de l’indivisaire décédé est calculée selon les règles de [l’article 15](#article_15).

### Article 17
*Tout membre peut être exclu* à l’unanimité de répartition des voix défini à [l’article 10](#article_10). Le membre ne peut pas prendre part à ce vote.

Sa quote-part de droits lui sera remboursée selon les mêmes modalités que dans le cas d’un retrait (cf. [article 15](#article_15)).

## Comptabilité et répartition des produits
### <a name="article_17">Article 16</a>
L’exercice court du 1er janvier au 31 décembre. Par exception, le premier exercice s’étend de la date de création du club au 31 décembre de la même année.

À la lumière de l’état de répartition communiqué par le Président du club, l’intermédiaire financier dépositaire fera connaître au début de chaque année civile à chacun des membres du club le montant des revenus encaissés pour son compte au cours de l’exercice précédent et lui fera remettre le certificat d’avoir fiscal ou de crédit d’impôt correspondant. **Les produits financiers générés par les investissements du club seront distribués. Dans le cas où ceux-ci sont reversés au club par les membres, ils constituent un versement volontaire**. À ce titre, ils doivent être pris en compte pour l’appréciation du plafond de versement autorisé.

**Le trésorier déterminera au fur et à mesure des investissements une valeur moyenne pondérée d’acquisition des titres** dans les conditions prévues par l’instruction du 19 septembre 1978 (BODGI 5G-7-78 par. 90) et la communiquera au(x) membre(s) lors de son (leur) retrait ainsi qu’à chaque membre au moment de la dissolution du club.

## Dissolution
### Article 17
**Le club sera dissout obligatoirement à l’échéance d’un délai de dix ans** à compter de sa date de création conformément à [l’article 1](#article_1) de la présente convention.

### <a name="article_19">Article 18</a>
**La dissolution anticipée du club peut être prononcée par décision des membres du club** statuant dans les conditions de [l’article 10](#article_10). Elle doit être prononcée dans l’hypothèse où le nombre de membres du club deviendrait inférieur à cinq.

### Article 19
**Lors de la dissolution, les avoirs du club sont répartis, après liquidation des investissements réalisés, entre les membres au prorata de leur quote-part de droits aux actifs**. Dans cette hypothèse, le gain net réalisé par les membres sera constitué par la différence entre le montant qui lui est remboursé par le club et le total des versements volontaires (intérêts et dividendes compris si ceux-ci sont reversés sur le compte du club) qu’il a effectués à la date de la dissolution. Pour apprécier le seuil d’imposition, le montant des sommes reçues du club doit être ajouté au total des cessions réalisées par chaque membre, au niveau de son foyer fiscal. En revanche, en cas de partage du portefeuille sans liquidation, aucune imposition ne sera due au moment du partage, à l’exception d’une éventuelle [soulte](https://fr.wikipedia.org/wiki/Soulte) immédiatement taxable. Toutefois, en cas de cession ultérieure de ses titres par le membre attributaire des titres partagés, les gains nets imposables seront calculés par référence au prix d’acquisition moyen des titres déterminé au niveau du club.

## Acte constitutif
### Article 20
Les membres du bureau sont :

* **Président** : Stanislas Signoud
* **Trésorier** : Nicolas Bouilleaud
* **Secrétaire** : Tristram Gräbener

### Article 21
Après avoir approuvé les présents statuts et en avoir accepté les termes, les personnes dont les noms suivent décident de fonder le club d’investissement **Les insécables**. Toute modification fera l’objet d’un avenant numéroté
